## 低代码
### 本质 - 降本增效
1. 降低开发成本
=> a. 统一组件库复用 b. 提升系统一致性 c. 合理化需求对齐页面
=> 降低人力开发成本
=> 
a. 建立低代码物料堆（直连、整合）
经典低代码:
el-form、el-form-item
el-input
zw-form-input

经典零代码:
zw-form
zw-input

b. 系统交互规范（交互、UI、逻辑）—— 原子设计

c. 开发投入流程

2. 传统型配置平台
Dreamweaver、xcode、IDE -- 面向骨架样式型配置工具

3. no-code & low-code 混合型提升效率的平台

### 课后
1. 三大件 => 仓库隔离
2. 渲染引擎 => 1. 网络层 2. 数据层 3. wrapper、装饰器 style
3. low-code no-code

